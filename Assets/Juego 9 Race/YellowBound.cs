﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowBound : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D other)
	{
		if (Track.isplayerdead == false) {
			if (other.gameObject.tag == "YellowScore") {
				Destroy (other.gameObject);
				ScoreSpawn.gameover++;
			}
		}
	}
}
