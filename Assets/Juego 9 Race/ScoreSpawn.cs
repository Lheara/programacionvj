﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreSpawn : MonoBehaviour {
	private float delayTimer = 0.9f;
	private float timer;
	[SerializeField] GameObject[] scoreprefabs;
	[SerializeField] Vector2 [] spawnpositions;
	private int randomStars;
	private int randomPos;
	public static int scorecount = 0;
	[SerializeField]Text scoretext;
	[SerializeField] Text gameovertext;
	public static int gameover = 0;
	[SerializeField]Image redpanel;
	[SerializeField]Image button;
	[SerializeField]Text buttontx;


	// Use this for initialization
	void Start () {
		timer = delayTimer;
		gameovertext.enabled = false;
		redpanel.enabled = false;
		button.enabled = false;
		buttontx.enabled = false;
	
	}
	
	// Update is called once per frame
	void Update () {
		scoretext.text = "Score: " + scorecount;
		timer -= Time.deltaTime;
		if (Track.isplayerdead == false) {
			if (timer <= 0) {
				randomPos = Random.Range (0, spawnpositions.Length);
				randomStars = Random.Range (0, scoreprefabs.Length);
				Instantiate (scoreprefabs [randomStars], spawnpositions [randomPos], Quaternion.identity);
				timer = delayTimer;
			}
			if (gameover == 1) {
				gameovertext.enabled = true;
				redpanel.enabled = true;
				//Time.timeScale = 0;
				button.enabled = true;
				buttontx.enabled = true;
				Track.isplayerdead = true;


			}
		}
		}

	public void RestartGame(){
		SceneManager.LoadScene ("Restart",LoadSceneMode.Single);
		Track.isplayerdead = false;
		gameovertext.enabled = false;
		redpanel.enabled = false;
		button.enabled = false;
		buttontx.enabled = false;
		gameover = 0;
		scorecount = 0;



	}
}

