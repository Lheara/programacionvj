﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowCar : MonoBehaviour {
	private float boundY =-1;
	private float boundY2 =-3;
	private Vector2 newPos;
	private float speed = 5;
	// Use this for initialization


	void Start () {
		newPos = transform.position;
	}
		
	
	// Update is called once per frame
	void Update () {
		if (Track.isplayerdead == false) {
			PositiongChanging ();
		}
	
		Vector2 pos = transform.position;
		pos.x = Mathf.Clamp (pos.x, boundY2, boundY);
		transform.position = pos;
	}
	void OnTriggerEnter2D(Collider2D other){
		if (Track.isplayerdead == false) {
			if (other.gameObject.tag == "YellowScore") {
				Destroy (other.gameObject);
				ScoreSpawn.scorecount++;
			}
			if (other.gameObject.tag == "BlueScore") {
				Destroy (other.gameObject);
				ScoreSpawn.gameover++;

		
			}

		}
	}
	void PositiongChanging(){
		Vector2 posA = new Vector2 (-1, -3);
		Vector2 posB = new Vector2 (-3, -3);

		if (Input.GetKeyDown (KeyCode.A)) {
			newPos = posB;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			newPos = posA;
		}
		transform.position = Vector2.Lerp (transform.position, newPos, Time.deltaTime*speed);
	}
}

