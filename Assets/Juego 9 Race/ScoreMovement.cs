﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreMovement : MonoBehaviour {
	private float speed =3f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (0, -1.5f * speed * Time.deltaTime, 0);
	}

	void OnBecameInvisible(){
		Destroy (gameObject);
	}

}
