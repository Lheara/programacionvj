﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBounds : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D other)
	{
		if (Track.isplayerdead == false) {
			if (other.gameObject.tag == "BlueScore") {
				Destroy (other.gameObject);
				ScoreSpawn.gameover++;
			}
		}
	}
}
