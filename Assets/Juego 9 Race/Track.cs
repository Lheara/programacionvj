﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Track : MonoBehaviour {
	private float speed = 2.5f;
	private Vector2 offset;
	public static bool isplayerdead;



	// Use this for initialization
	void Start () {
		isplayerdead = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (isplayerdead == false) {
			offset = new Vector2 (0, Time.time * speed);
			GetComponent<Renderer> ().material.mainTextureOffset = offset;
		}

	
	}
}

