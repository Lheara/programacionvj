﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCubes : MonoBehaviour {
	[SerializeField] GameObject prefab;
	// Use this for initialization
	void Start () {
		Invoke ("CubosEscena", 1);
		
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (new Vector3 (15, 30, 45) * Time.deltaTime);
		
	}
	void CubosEscena(){
		for (int cont = 0; cont <= 9; cont++) {
			Vector3 posicion = new Vector3 (Random.Range (-8.3f,8.3f), -0.9f, Random.Range (14.7f, -2.4f));
			Instantiate(prefab,posicion,Quaternion.Euler(45,45,45));
		}
	}
}
