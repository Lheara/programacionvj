﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMov : MonoBehaviour {
	private Rigidbody rb;
	private float speed = 10;
	private int scorecount =0;
	[SerializeField] Text score;
	[SerializeField] Text win;
	[SerializeField] Image panel;
	[SerializeField] Text gameover;
	[SerializeField]Image panel2;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		panel.enabled = false;
		gameover.enabled = false;
		panel2.enabled = false;
		
	}
	
	// Update is called once per frame
	void Update () {
		float horizontal = Input.GetAxis ("Horizontal");
		float vertical = Input.GetAxis ("Vertical");

		Vector3 mov = new Vector3 (horizontal,0, vertical);	
		rb.AddForce (mov * speed);
		winN ();
		
	}

	void winN(){
		if (scorecount == 10) {
			panel.enabled = true;
			win.enabled = true;
			Time.timeScale = 0;
		}
	}

	void OnCollisionEnter(Collision cubes){
		if (cubes.gameObject.tag == "limit") {
			panel2.enabled = true;
			gameover.enabled = true;
			Destroy (gameObject);
			Time.timeScale = 0;

		}

	}

		void OnTriggerEnter (Collider cube){
		if (cube.gameObject.tag == "enemy") {
			scorecount++;
			score.text = "Score: " + scorecount;
			Destroy (cube.gameObject);
		}
	}


	}
