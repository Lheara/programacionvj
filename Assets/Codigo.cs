﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Codigo : MonoBehaviour {
	public int contador;
	[SerializeField] Text resultado;

	// Use this for initialization
	void Start () {
		contador = 0;
		
	}
	
	// Update is called once per frame
	void Update () {

	}
	public void Suma(){
			contador++;
			resultado.text = contador.ToString();
		}

	public void Resta(){
			contador--;
			resultado.text = contador.ToString();
		}

}
