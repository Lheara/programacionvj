﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BirdMovement : MonoBehaviour {
	private Rigidbody2D rigid;
	private float minJump;
	private float maxJump;
	private float accPressure;
	[SerializeField] Text gameovertext;
	[SerializeField] Text scoretext;
	[SerializeField] Image panel;
	private int score=0;

	// Use this for initialization
	void Start () {
		gameovertext.enabled = false;
		panel.enabled = false;
		rigid = GetComponent<Rigidbody2D> ();
		accPressure = 0f;
		minJump = 1f;
		maxJump = 15f;

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(Input.GetMouseButton(0)){
			if (accPressure < maxJump) {
				accPressure += Time.deltaTime * 15f;
			} else {
				accPressure = maxJump;

			}

		} else {
			if (accPressure > 0f) 
			{
				accPressure = accPressure + minJump;
				rigid.velocity = new Vector3 (accPressure / 10f, accPressure, 0f);
				accPressure = 0f;	

		}
	}
	}

	void OnTriggerEnter2D (Collider2D other){
		if (other.gameObject.CompareTag ("Enemy")) {
			gameovertext.enabled = true;
			panel.enabled = true;
			DEADD ();
		}
		if (other.gameObject.CompareTag ("ScoreZone")) {
			score++;
			scoretext.text = "Score: " +score;
		}
	}

	void OnBecameInvisible(){
		DEADD ();


	}
	void DEADD(){
		gameovertext.enabled = true;
		panel.enabled = true;
		Time.timeScale = 0;
	}
}
