﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSpawn : MonoBehaviour {
	[SerializeField] GameObject pipes;
	[SerializeField] GameObject pipesdown;
	private float timer;


	void Awake () {
		timer = Time.time + 2;
		
	}
	
	// Update is called once per frame
	void Update () {
		
			if (timer < Time.time) {
				Vector2 enemyPos = new Vector2 (transform.position.x, Random.Range (5.8f, 7f));
				Vector2 enemyPos2 = new Vector2 (transform.position.x, Random.Range (-5.8f, -9f));
				Instantiate (pipes, enemyPos, Quaternion.identity);
				Instantiate (pipesdown, enemyPos2, Quaternion.Euler (0, 0, 180));
				timer = Time.time + 2;

		}
	}
}
		
			



