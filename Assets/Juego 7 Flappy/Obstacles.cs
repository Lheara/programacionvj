﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : MonoBehaviour {
	private float speed = -1.5f;
	private Rigidbody2D rigid;


	// Use this for initialization
	void Start () {
		
			rigid = GetComponent<Rigidbody2D> ();
			rigid.velocity = new Vector2 (speed, 0);


	}

	void OnBecameInvisible(){
		Destroy (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
		}
		
	}
