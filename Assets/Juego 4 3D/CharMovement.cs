﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharMovement : MonoBehaviour {
	private Rigidbody rigid;
	private float minJump;
	private float maxJump;
	private bool onGround;
	[SerializeField]private float accPressure;
	[SerializeField] GameObject ground;
	[SerializeField]private GameObject laststep;
	[SerializeField] Text score;
	[SerializeField] int contador;
	[SerializeField] Text gameover;
	[SerializeField] GameObject camera;


	// Use this for initialization
	void Start () {
		accPressure = 0f;
		minJump = 2f;
		maxJump = 15f;
		rigid = GetComponent<Rigidbody> ();
		onGround = true;



		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (onGround)
		if (Input.GetButton ("Jump")) {
				if (accPressure < maxJump) {
					accPressure += Time.deltaTime * 10f;
				} else {
					accPressure = maxJump;

				}
			
			} else {
				if (accPressure > 0f) 
				{
					accPressure = accPressure + minJump;
					rigid.velocity = new Vector3 (accPressure / 10f, accPressure, 0f);
					accPressure = 0f;
				onGround = false;
				Destroy (ground);
				Destroy(laststep);

				}
				
			}
			
	}


	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.CompareTag ("ground")) {
			onGround = true;
			laststep = other.transform.gameObject;
			contador += 100; 
			score.text = "Score: " + contador;
			Instantiate (laststep, new Vector3(transform.position.x+4f, transform.position.y+3f,transform.position.z), Quaternion.Euler(0,90,0));
		}
	}
	void OnTriggerEnter(Collider other){
		gameover.enabled = true;
		camera.transform.parent = null;
		Debug.Log ("gameover");
	}
}



