﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	[SerializeField] GameObject player;
	private Vector3 cameraposition;

	// Use this for initialization
	void Start () {
		cameraposition = transform.position - player.transform.position;
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = player.transform.position + cameraposition;
		
	}
}
