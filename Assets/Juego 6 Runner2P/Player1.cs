﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player1 : MonoBehaviour {
	private Rigidbody rb;
	[SerializeField] Text timer;
	private float timercount = 0.0f;
	private bool onGround;
	[SerializeField] GameObject explosion;

	// Use this for initialization
	void Start () {

		rb = GetComponent<Rigidbody> ();
		onGround = true;
	}

	// Update is called once per frame
	void Update () {
		if (EnemySpawwnn2.isPlayerDead == false) {
			timercount += Time.deltaTime;
			timer.text = "Time: " + timercount;
			if (Input.GetKeyUp (KeyCode.LeftArrow)) {
				transform.Translate (-1f, 0, 0);
			}
			if (Input.GetKeyUp (KeyCode.RightArrow)) {
				transform.Translate (1f, 0, 0);
			}

			if (onGround)
			if (Input.GetKey (KeyCode.UpArrow)) {
				rb.velocity = new Vector3 (0, 8f, 0);
				onGround = false;
			}
		}

	}
	void OnCollisionEnter (Collision other){


		if (other.gameObject.CompareTag ("ground")) {
			onGround = true;
		}
		if (other.gameObject.CompareTag ("Enemy")) {
			Instantiate (explosion, transform.position, Quaternion.Euler(0,-27,0));
			Destroy (gameObject);
			gmanager.gameover++;

		}
	}

	void OnBecameInvisible(){
		Destroy (gameObject);
		gmanager.gameover++;
	}

}
