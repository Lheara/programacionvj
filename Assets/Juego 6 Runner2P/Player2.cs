﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player2 : MonoBehaviour {
	private Rigidbody rb;
	private bool onGround;
	[SerializeField] GameObject explosion;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		onGround = true;

		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.A)){
			transform.Translate (-1f, 0, 0);
		}
		if (Input.GetKeyUp (KeyCode.D)) {
			transform.Translate (1f, 0, 0);
		}

		if (onGround)
			if (Input.GetButton("Jump")){
			rb.velocity = new Vector3 (0, 8f, 0);
			onGround = false;
		}
		
	}
	void OnCollisionEnter (Collision other){
		if (other.gameObject.CompareTag ("Player")) {
			transform.Translate (-1f, 0, 0);
		}
		if (other.gameObject.CompareTag ("ground")) {
			onGround = true;
		}
		if (other.gameObject.CompareTag ("Enemy")) {
			Instantiate (explosion, transform.position, Quaternion.Euler(0,-27,0));
			gmanager.gameover++;
			Destroy (gameObject);


		}
	}

	void OnBecameInvisible(){
		Destroy (gameObject);
		gmanager.gameover++;
	}


}
