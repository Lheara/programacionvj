﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controllers : MonoBehaviour {
	[SerializeField] float speed = 0;
	[SerializeField] Transform rightpaddle;
	[SerializeField] Transform leftpaddle;

	// Use this for initialization
	void Start () {
		
		
	}

	// Update is called once per frame
	void Update () {
		
		if (Input.GetKey (KeyCode.UpArrow)) {
			rightpaddle.Translate (Vector2.up * Time.deltaTime * speed);

		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			rightpaddle.Translate (Vector2.down * Time.deltaTime * speed);

		}
		if (Input.GetKey (KeyCode.W)) {
			leftpaddle.Translate (Vector2.up * Time.deltaTime * speed);

		}
		if (Input.GetKey (KeyCode.S)) {
			leftpaddle.Translate (Vector2.down * Time.deltaTime * speed);

		}

	}

		}



