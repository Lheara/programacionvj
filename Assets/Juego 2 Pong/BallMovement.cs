﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallMovement : MonoBehaviour {
	[SerializeField] UIManager uimanager;
	[SerializeField]float speed = 0;
	float hitFactor (Vector2 ballPos, Vector2 stackPos, float paddleheight){
		return (ballPos.y - stackPos.y) / paddleheight;
	}

		
	// Use this for initialization
	void Start () {
		Invoke ("BallMove", 2);
	}
		
		
	void BallMove(){
		GetComponent<Rigidbody2D> ().velocity = Vector2.right * speed;
	}
	void ResetBall(){
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, 0f);
		transform.position = Vector2.zero;
		Invoke ("BallMove", 2);
	}
		
	
	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.name == "LeftPaddle") {
			float y = hitFactor (transform.position,
				          col.transform.position, col.collider.bounds.size.y);
			Vector2 dir = new Vector2 (1, y).normalized;
			GetComponent<Rigidbody2D> ().velocity = dir * speed;
		}
		if (col.gameObject.name == "RightPaddle") {
			float y = hitFactor (transform.position, col.transform.position, col.collider.bounds.size.y);
			Vector2 dir = new Vector2 (-1, y).normalized;
			GetComponent<Rigidbody2D> ().velocity = dir * speed;
		}
	}


	void OnTriggerEnter2D(Collider2D choque){
		if (choque.name == "WallLeft") {
			uimanager.Updateleftcounter ();
			ResetBall ();

		}
		if (choque.name == "WallRight") {
			uimanager.Updaterightcounter ();
			ResetBall ();

		}
	}
	 

}
	
