﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class movimiento : MonoBehaviour {
	private Rigidbody2D rigid; 
	public float speed = 10.0f;
	public int contador = 0;
	[SerializeField] Text score;



	// Use this for initialization
	void Start () {
		rigid = GetComponent<Rigidbody2D> ();

	
	}
	void Update(){
		if (contador == 10) {
			Time.timeScale = 0;
		}
	}

	
	// Update is called once per frame
	void FixedUpdate () { 
		float horizontal = Input.GetAxis ("Horizontal");
		float vertical = Input.GetAxis ("Vertical");

		rigid.velocity = new Vector2 (horizontal*speed*Time.deltaTime, vertical*speed*Time.deltaTime);

		}
		
		

	void OnTriggerEnter2D (Collider2D cupcake){
		if (cupcake.gameObject.tag == "cupcake") {
			Destroy (cupcake.gameObject);
			contador++;
			score.text = "Score: " + contador;
		}
	}



}
	
