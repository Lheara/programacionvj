﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigEscena : MonoBehaviour {
	public GameObject cupcake;
	int maximo = 9;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void inicio(){
		for (int contador = 0; contador <= maximo; contador++) {
			Vector2 posicion = new Vector2(Random.Range(-7.0f, 7.0f), Random.Range(-4.0f,4.0f));
				Instantiate(cupcake, posicion, Quaternion.identity);
		}
		
	}

}
