﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyClean : MonoBehaviour {
	[SerializeField] Text gameover;



	void OnCollisionEnter (Collision other){
		if (other.gameObject.tag == "Enemy") {
			Destroy (other.gameObject);
		}
	}
}
