﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyMovement : MonoBehaviour {
	private Rigidbody rigid;
	private float speed = 0f;
	private float speedAcc = 1.5f;

	// Use this for initialization
	void Start () {
		rigid = GetComponent<Rigidbody> ();
		speed += Time.deltaTime;



	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (EnemySpawwnn.isPlayerDead == false) {
			speed += speedAcc * Time.deltaTime;
			rigid.velocity = (new Vector3 (0, 0, -4f * speed));
		}
	}
	}


