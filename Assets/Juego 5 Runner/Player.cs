﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {
	private Rigidbody rb;
	[SerializeField] Text timer;
	[SerializeField] Text GameOver;
	private float timercount = 0.0f;
	private bool onGround;
	[SerializeField] Image panel;
	[SerializeField] GameObject explosion;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		onGround = true;
		GameOver.enabled = false;
		
	}
	
	// Update is called once per frame
	void Update () {
		timercount += Time.deltaTime;
		timer.text = "Time: " + timercount;
		if (Input.GetKeyUp(KeyCode.A)){
			transform.Translate (-1f, 0, 0);
		}
		if (Input.GetKeyUp (KeyCode.D)) {
			transform.Translate (1f, 0, 0);
		}

		if (onGround)
			if (Input.GetButton("Jump")){
			rb.velocity = new Vector3 (0, 8f, 0);
			onGround = false;
		}
		
	}
	void OnCollisionEnter (Collision other){
		if (other.gameObject.CompareTag ("ground")) {
			onGround = true;
		}
		if (other.gameObject.CompareTag ("Enemy")) {
			Instantiate (explosion, transform.position, Quaternion.Euler(0,-27,0));
			Destroy (gameObject);
			GameOver.enabled = true;
			panel.enabled = true;
			EnemySpawwnn.isPlayerDead = true;

		}
	}

	void OnBecameInvisible(){
		Destroy (gameObject);
		GameOver.enabled = true;
		panel.enabled = true;
		EnemySpawwnn.isPlayerDead = true;
	}


}
