﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

	private Transform  bullet;
	public float speed;
	// Use this for initialization
	void Start () {
		bullet = GetComponent<Transform> ();
	}

	void FixedUpdate(){
		bullet.position += Vector3.up * -speed;
	}

	void OnBecameInvisible() {
		Destroy (gameObject);
	}
		
			
		
	
	// Update is called once per frame
	void Update () {
		
	}
}
