﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {
	public GameObject enemy;

	// Use this for initialization
	void Start () {
		Instantiate (enemy, new Vector2 (0, 0), Quaternion.identity);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
