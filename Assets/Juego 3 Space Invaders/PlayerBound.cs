﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBound : MonoBehaviour {
	private float boundX = 7.8f;
	private Rigidbody2D rigid;

	// Use this for initialization
	void Start () {
		rigid = GetComponent<Rigidbody2D>();
		
	}
	
	// Update is called once per frame
	void Update () {
		var vel = rigid.velocity;
		var pos = transform.position;
		if (pos.x > boundX) {
			pos.x = boundX;
		} else if (pos.x < -boundX) {
			pos.x = -boundX;
		}
		transform.position = pos;
	}
}
