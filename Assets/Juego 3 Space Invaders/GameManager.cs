﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	public static GameManager GM;
	public float playerScore = 0;
	public Text scoreText;
	public Text txhealth;
	public int health = 5;


	void Awake()
	{
		if (GM == null)
		{
			GM = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			Destroy(gameObject);
		}
	}

	void Update () {
		scoreText.text = "Score: "+playerScore;
		txhealth.text = "Vidas: " + health;
}
}
			

   
	

