﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHealth : MonoBehaviour {
	public float health = 4;

	void Update () {
		if (health <= 0)
			Destroy (gameObject);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "EnemyBullet") {
			health--;
			Destroy (other.gameObject);

		}
	}
}
