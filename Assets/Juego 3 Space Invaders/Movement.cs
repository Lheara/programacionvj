﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour {
	float speed = 5;
	[SerializeField] GameObject bullets;
	[SerializeField] float NextShot;
	[SerializeField] float FireRate;




	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey (KeyCode.RightArrow)){
			transform.Translate (Vector2.right * speed * Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.LeftArrow)) {
			transform.Translate (Vector2.left * speed * Time.deltaTime);
		}
		if (Input.GetKeyDown (KeyCode.Space) && Time.time>NextShot){
			NextShot = Time.time + FireRate;
			
		Instantiate (bullets,transform.position, Quaternion.identity);
		}
		if (GameManager.GM.health<= 0) {
			GameOver.isPlayerDead = true;
		}

		
	}
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "EnemyBullet") {
			GameManager.GM.health--;
		}
	}

		
	}


